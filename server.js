var server = require('http').createServer();
var port = 8080;
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var fs = require('fs');
mongoose.connect('mongodb://localhost/thesis22');


app.use(express.static(__dirname + '/assets'));

// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));
var jsonParser = bodyParser.json()

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Database connected!");
});

/**
 * { Mongoose schemes }
 *
 */
var Schema = mongoose.Schema;
var VideoScheme = new Schema({
    frames:Array,
    mostDelayedQuad: Number,
    sync:Boolean,
    number_quads_delayed: Number,
    total_delayed_frames: Number,
    quadrants: Array,
    differenceFrames: Array});

var qrSchema = new Schema({
	total_media_delayed_frames: Number,
	total_media_delayed_quads: Number,
	frames_recognized: Number,
	video_informations:[VideoScheme]
});

var QRDB = mongoose.model('qrInfoDb', qrSchema);


var fileCount = 0;
app.get('/', function(request, response){
	QRDB.find(function(err, infos){
		if (err) return console.error(err);
		console.log(infos);
		response.send(infos);
	})
});

var fileCount = 0;
app.get('/results2', function(request, response){
	QRDB.find(function(err, infos){
		if (err) return console.error(err);
		var temp = JSON.stringify(infos);
		temp = JSON.parse(temp);
		temp = JSON.stringify(temp);
		var objects = JSON.parse(temp);
		var result;
		
		temp = JSON.stringify(objects[1].total_media_delayed_frames);
		console.log(temp);
		response.send(temp);
	})
});

var infoObject = function(quads, frames, recognized){
	this.total_media_delayed_quads = quads.substring(0, quads.indexOf('.') + 3);
	this.total_media_delayed_frames = frames.substring(0, frames.indexOf('.') + 3);
	this.frames_recognized = recognized;
}

var allTestsMedia = function(totalMediaTestFrames, totalMediaDelayedQuads, infoObject){
	this.totalMediaTestFrames = totalMediaTestFrames;
	this.totalMediaDelayedQuads = totalMediaDelayedQuads;
	this.infoObject = infoObject;
}


app.get('/results', function(request, response){
	QRDB.find({},'total_media_delayed_frames total_media_delayed_quads frames_recognized', function(err, infos){
		if (err) return console.error(err);
		var tempArr = JSON.stringify(infos);
		var objects = JSON.parse(tempArr);
		var totalMediaTestFrames = 0;
		var totalMediaDelayedQuads = 0;
		var endTotalFramesRecognized = 0;
		var infoAnalyzeArr = new Array()
		var infoAnalyzeObject;
		var fileStructure = "";
		for(var count = 0 ; count < objects.length; count++){
			totalMediaTestFrames += objects[count].total_media_delayed_frames;
			totalMediaDelayedQuads +=  objects[count].total_media_delayed_quads;
			
			infoAnalyzeObject = new infoObject(''+objects[count].total_media_delayed_quads,
				''+objects[count].total_media_delayed_frames, objects[count].frames_recognized);
			infoAnalyzeArr.push(infoAnalyzeObject);
			
			fileStructure += infoAnalyzeObject.total_media_delayed_quads + ';' + 
			infoAnalyzeObject.total_media_delayed_frames + ";" + infoAnalyzeObject.total_media_delayed_frames + 
			"\n";
		}
		totalMediaTestFrames /= objects.length;
		totalMediaDelayedQuads /= objects.length;
		var finalResult = new allTestsMedia(totalMediaTestFrames, totalMediaDelayedQuads, infoAnalyzeArr);
		json = JSON.parse(JSON.stringify(finalResult));
		writeFile(fileStructure);
		console.log(json);
		response.send(json);
	})
});

var date;
var testCount = 0;
/**
 * { POST from server }
 */
app.post('/', jsonParser, function (req, res){
	var info = new QRDB(req.body)
	info.save(function(err, info){
		console.log("saving");
		if (err) return console.error(err); 
	})
	res.send('201');
})


server.on('request', app);
server.listen(port, function(){
	console.log("Listening on " + server.address().port);
});

function writeFile(obj){
	date = new Date();
	testCount++;
	fs.writeFile('TestsFiles/SyncThesis - ' + testCount + ' date: ' + date + '.txt', 
	 	obj, function (err) {
        if (err) throw err;
        console.log('It\'s saved! in same location.');
    });
}
